import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static void main(String[] argv) {
        List<List<Post>> rawData = new JsonParser().getPostsLists();
        if (rawData == null || rawData.isEmpty()) {
            throw new NullPointerException("Json data is null, please contact Ilya to solve this problem");
        }
        List<Post> posts = new Sorter().sort(rawData);
        DateFormat formatter = new SimpleDateFormat("dd.MM.YYYY");
        for (Post post : posts) {
            Date date = new Date(post.date);
            System.out.println(post.id + " " + formatter.format(date));
            System.out.println(post.text);
            System.out.println();
        }
    }
}
