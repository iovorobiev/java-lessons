import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class JsonParser {

    public List<List<Post>> getPostsLists() {
        try {
            Gson gson = new Gson();
            Post[] ilyasPosts = gson.fromJson(new BufferedReader(new InputStreamReader(new FileInputStream("ilya.json"))), Post[].class);
            Post[] lenasPosts = gson.fromJson(new BufferedReader(new InputStreamReader(new FileInputStream("lena.json"))), Post[].class);
            Post[] stepansPosts = gson.fromJson(new BufferedReader(new InputStreamReader(new FileInputStream("stepan.json"))), Post[].class);
            List<List<Post>> all = new LinkedList<List<Post>>();
            all.add(Arrays.asList(ilyasPosts));
            all.add(Arrays.asList(lenasPosts));
            all.add(Arrays.asList(stepansPosts));
            return all;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
